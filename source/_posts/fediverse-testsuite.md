
---
layout: "post"
title: "Fediverse testsuite"
date: 2018-03-15
tags:
    - fediverse
preview: "This tool runs CI tests in addition to the project's own testsuite and will help developers working on federated platforms spot bugs easier."
url: "https://libranet.de/display/0b6b25a8195aa9bf638d22f150546550"
lang: en
---

This tool runs CI tests in addition to the project's own testsuite, to help developers working on platforms federating with each other spot bugs easier.
More info [here](https://libranet.de/display/0b6b25a8195aa9bf638d22f150546550).
