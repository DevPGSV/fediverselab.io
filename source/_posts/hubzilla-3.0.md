
---
layout: "post"
title: "Hubzilla 3.0 major release"
date: 2018-01-11
tags:
    - hubzilla
preview: "Dedicated to the memory of Tony Baldwin (@Tazman), a Friendica / Redmatrix / Hubzilla contributor, who passed away last week."
url: "https://ioh.hfrc.de/display/6144c0d9b03e8b370e243e2fa83900c3649248591efc0295babb7f7e50cf1fb8@ioh.hfrc.de"
lang: en
---

Dedicated to the memory of Tony Baldwin (@Tazman), a Friendica / Redmatrix / Hubzilla contributor, who passed away last week.
More info [here](https://ioh.hfrc.de/display/6144c0d9b03e8b370e243e2fa83900c3649248591efc0295babb7f7e50cf1fb8@ioh.hfrc.de).
