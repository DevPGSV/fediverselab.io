
---
layout: "post"
title: "Aardwolf Mozilla Global Sprint"
date: 2018-03-22
tags:
    - aardwolf
preview: "Aardwolf is taking part in Mozilla’s Global Sprint (May 10th-11th). Check out the hacker's guide of the project for all the current plans and ideas."
url: "https://aardwolf.social/news/2018/03/21/hackers-guide-to-mozilla-sprint"
lang: en
---

Aardwolf is taking part in Mozilla’s Global Sprint (May 10th-11th). The entry point will be the Aardwolf hacker's guide which is a top-level directory used as a library for documenting the development targets/features.
Check it out [here](https://aardwolf.social/news/2018/03/21/hackers-guide-to-mozilla-sprint).
