---
layout: "post"
title: "PeerTube stable 1.0.0 [updated]"
date: 2018-10-12
tags:
    - peertube
preview: "PeerTube reached first stable release. A great ammount of work has been put into the project, most milestones met, new features ahead"
url: "/en/post/peertube-stable-1.0.0/"
lang: en
---

PeerTube, federated video streaming platform, reached first [stable release](https://github.com/Chocobozzz/PeerTube/releases/tag/v1.0.0). It's in the final testing stage, the official announcement is due Monday.

Following the successful [crowdfunding campaign](https://www.kisskissbankbank.com/en/projects/peertube-a-free-and-federated-video-platform) a great ammount of work has been put into the project. It was polished, improved and enforced with new features. Most [milestones](https://github.com/Chocobozzz/PeerTube/milestone/1?closed=1) were met. Among other things, PeerTube now has user subscriptions, subtitles and localization support, [advanced search](https://github.com/Chocobozzz/PeerTube/issues/60), QR code to share video's URL, scheduling a video update, adding NSFW videos policy, ability to delete your account, blacklist local videos or set a moderation comment to an abuse, ability to import videos from a URL (YouTube, Dailymotion, Vimeo, raw file etc), remote comments from other Fediverse platforms like Mastodon, Pleroma etc, magnet URIs in player and download modal.

Some well-known organizations, including [Blender](https://video.blender.org/accounts/blender), [Krita](https://share.tube/accounts/kritafoundation/videos), [GNOME](https://peertube.video/accounts/gnome/videos) and [KDE](https://peertube.video/accounts/kde/videos) have officially joined PeerTube. See updating list of early adopters [here](https://fediverse.party/en/peertube).

Would you like to see more video creators in PeerTube? Invite them to join!
