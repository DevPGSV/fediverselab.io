
# CONTRIBUTING

Don't hesitate to send a merge request!

Two easy and most welcome ways to contribute are:
1. submitting short news to The Chronicles
2. writing an internal article for the website

### Submitting news

**Data located in**: `/source/_posts` folder

**[Chronicles](https://fediverse.party/en/chronicles)** page aggregates latest news about major releases, development, interviews, related projects of Fediverse *social networks* (i.e., all the networks listed on main page).

Posts are available via [RSS](https://fediverse.gitlab.io/atom.xml) subscription.
`Preview` is rendered on Chronicles page (limited ammount), text after metadata is shown in RSS (unlimited).

Every post **must have** the following __metadata__:

```
layout: "post"
title: "some title"
date: 2222-01-25
tags:
    - pleroma
preview:
  "short gist..."
url: "https://pleroma.social/link-to-news-source"
lang: en
```

`Tags`
A post may have one of these tags: fediverse, gnusocial, diaspora, friendica, hubzilla, mastodon, postactiv, pleroma, socialhome, ganggo, misskey, peertube, aardwolf

`Preview`
Limit 150 characters - for "regular" and "wanted" posts, limit 350 characters - for "featured" post.
Please, make preview a 150-characters note (350-characters for "featured"), otherwise it gets truncated half-sentence and will be posted that way on the Chronicles front page. Not good.

__Optional metadata__:

```
wanted: true
featured: true
banner: "pic.jpg"
authors: [{"name": "John Snow", "url": "https://ggg.social", "network": "socialhome"}]
```

`Wanted`
Add this metadata to a post that you wish to show in the upper visible part of the Chronicles page. Calls for contribution, donations, help should be posted with this metadata.

`Featured`
Add this metadata to a new internal website's article, to show the post in a prominent part of the Chronicles page.

`Wanted` and `featured` can't be mixed and are temporary. This metadata must be removed from an older post when creating a new "featured" or "wanted" post.

`Banner`
Required for "featured" posts only, an image wide enough to be used as a fullscreen background, should be placed in `/source/_posts/exact-post-file-name` folder. See [example](./source/_posts/why-use-federated-networks).

`Authors`
Required for "featured" posts only. Add an array of object(s): name you wish to be shown as the author, and one website link. If it's the link to yout account on Fediverse, specify network name - lower case, without spaces.

### Writing an internal article

Please, feel free to work on [FAQ](https://fediverse.party/en/post/FAQ-for-newcomers), [why use federated networks](https://fediverse.party/en/post/why-use-federated-networks) stub articles, or submit a piece dedicated to the topic of your own choosing (related to Fediverse social networks).

Each new internal article will be posted as "featured" in the prominent part of the Chronicles page, and will stay there for a while. It will also be distributed via RSS subscription. Your name and avatar will be featured as the author(s).

Please, consider submitting an original text that hasn't yet been published elsewhere. If you have a good relevant post on your own blog, submit it as a link addition to any Network page or Fediverse page. It doesn't make much sense to duplicate content.

A merge request with an article will include all the things mentioned above for a `featured` post: a file placed in `/source/_posts` folder with necessary metadata, a 350-character preview and full text after metadata section, written in markdown. A banner image should be added to `/source/_posts/exact-post-file-name` folder that you'll create. Any other images used in the article may be placed there as well.

**Thanks in advance!**
